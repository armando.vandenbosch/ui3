Usability Test van het UI3 Project met als testpersoon Jolien.

1. Wat is de temperatuur in de gang(=hallway) op het tweede verdieping (=second floor)?

15°C 

Observering:
het kiezen van een floor was lastig. De input is niet het eerst waar naar gekeken word. De applicatie startte eerst door de plattegrond te tonen. Dit is nu veranderd naar de lijst weergave waardoor de input sneller zichtbaar is. De titel boven de input is ook groter gemaakt om het beter te doen opvallen.

2. Verander het default volume in de keuken(=kitchen) op verdieping 1(=first floor) naar 15

gelukt

Observering:
Het verschil tussen de current en default volume was niet meteen duidelijk. Dit kan mogelijk zijn doordat het systeem in het engels is.

3. Stel in dat de verwarming in de livingroom op het 1ste verdieping(= first floor)  tussen 8 en 16u op 15 graden moet staan.

gelukt

Observering:
Alles was duidelijk, enkel mogelijke verwarring tussen knop "save timeslots" en "extra timeslot" aanmaken. Daarom is de knop voor het aanmaken van een timeslot en een extra timeslot outlined en anders dan de save knoppen.