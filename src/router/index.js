import Vue from 'vue'
import VueRouter from 'vue-router'
import Floors from '../views/Floors.vue'
import Login from '../views/Login.vue'
import RoomDetail from '../views/RoomDetail.vue'
import UserAdministration from '../views/UserAdministration.vue'

Vue.use(VueRouter)

const routes = [
{
    path: '/',
    name: 'Home',
    redirect: '/login/'
},
{
    path: '/login/',
    name: 'Login',
    component: Login
},
{
    path: '/floor/',
    name: 'Floors',
    component:  Floors
},
{
    path: '/admin/',
    name: 'Admin',
    component:  UserAdministration
},
{
    path:'/floor/:floorId',
    name: 'Floor',
    component: Floors
},
{
    path: '/:floorId/:roomName',
    name: 'roomName',
    component: RoomDetail,
    props: true
}
]

const router = new VueRouter({
    routes,
    mode:'history'
})

export default router