import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/timeslots'

export default {
    getByRoomId(roomId) {
        return axios.get(RESOURCE_PATH+'?room_id='+roomId).then(result => result.data)
    },
    put(timeslot){
        return axios.put(RESOURCE_PATH+'/'+timeslot.id,timeslot)
    },
    post(timeslot){
        return axios.post(RESOURCE_PATH,timeslot)
    },
    delete(timeslot){
        return axios.delete(RESOURCE_PATH+'/'+timeslot.id)
    }
}