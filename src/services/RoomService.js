import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/rooms'

export default {
    get() {
        return axios.get(RESOURCE_PATH).then(result => result.data)
    },
    getByFloorId(floorId) {
        return axios.get(RESOURCE_PATH + '?floor_id=' + floorId).then(result => result.data)
    },
    getByName(roomName){
        return axios.get(RESOURCE_PATH+'?name='+roomName).then(result => result.data)
    },
    getByNameAndFloorId(floorId,roomName){
        return axios.get(RESOURCE_PATH+'?name='+roomName+'&floor_id='+floorId).then(result => result.data)
    },
    put(room) {
        return axios.put(RESOURCE_PATH+'/'+room.id,room)
    }
}