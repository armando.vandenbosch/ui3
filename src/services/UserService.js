import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/users'

export default {
    getByNameAndPassword(name,password){
        return axios.get(RESOURCE_PATH+"?name="+name+"&password="+password)
    },
    get(){
        return axios.get(RESOURCE_PATH).then(result=>result.data)
    },
    delete(user){
        return axios.delete(RESOURCE_PATH+'/'+user.id)
    },
    put(user){
        return axios.put(RESOURCE_PATH+'/'+user.id,user)
    },
    add(user){
        return axios.post(RESOURCE_PATH,user)
    }
}