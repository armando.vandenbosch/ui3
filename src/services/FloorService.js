import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/floors'

export default {
    get() {
        return axios.get(RESOURCE_PATH).then(result => result.data)
    },
    getById(id){
        return axios.get(RESOURCE_PATH+'/'+id).then(result => result.data)
    }
}